Index
1. What is it?
2. How to use it.
-----------------

What is it?
For Darknet-yolo-v2 on windows( https://github.com/AlexeyAB/darknet#how-to-use-on-the-command-line), The training program need 'text file' that label writed for each jpg file. Like this <label> <x> <y> <width> <height>, This porgram can make the text file simply and easily.
----------------- 
 
How to use it.
 1. Excute program, then a window will be opened.
 2. Drag image file to the window, then automatically, Input and Output file name is filled in right up side editlines.
 3. Specify a range for label using left button and right button.
 4. Push the 'Push' button, then The program append a string that form is '<label> <x> <y> <width> <height>' to the right Contents.
 5. Push this 'Save' button, you can take a text file for that image file you input.
-----------------