/********************************************************************************
** Form generated from reading UI file 'MainWindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "myimagelabel.h"

QT_BEGIN_NAMESPACE

class Ui_MainWindowClass
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QScrollArea *ImageScrollArea;
    QWidget *ImageScrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_4;
    MyImageLabel *MyImage;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer;
    QPushButton *PushButton;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout_3;
    QFormLayout *formLayout;
    QLabel *label_2;
    QLineEdit *InputName;
    QLabel *label_3;
    QLineEdit *OutputName;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QScrollArea *ContentsScrollArea;
    QWidget *ContentsScrollAreaWidgetContents;
    QVBoxLayout *verticalLayout;
    QPlainTextEdit *Contents;
    QVBoxLayout *verticalLayout_6;
    QSpacerItem *verticalSpacer_3;
    QPushButton *ResetButton;
    QPushButton *SaveButton;
    QSpacerItem *verticalSpacer_4;

    void setupUi(QMainWindow *MainWindowClass)
    {
        if (MainWindowClass->objectName().isEmpty())
            MainWindowClass->setObjectName(QStringLiteral("MainWindowClass"));
        MainWindowClass->resize(1107, 474);
        MainWindowClass->setAcceptDrops(true);
        centralWidget = new QWidget(MainWindowClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAcceptDrops(true);
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        ImageScrollArea = new QScrollArea(centralWidget);
        ImageScrollArea->setObjectName(QStringLiteral("ImageScrollArea"));
        ImageScrollArea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
        ImageScrollArea->setWidgetResizable(true);
        ImageScrollArea->setAlignment(Qt::AlignCenter);
        ImageScrollAreaWidgetContents = new QWidget();
        ImageScrollAreaWidgetContents->setObjectName(QStringLiteral("ImageScrollAreaWidgetContents"));
        ImageScrollAreaWidgetContents->setGeometry(QRect(0, 0, 457, 454));
        verticalLayout_4 = new QVBoxLayout(ImageScrollAreaWidgetContents);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        MyImage = new MyImageLabel(ImageScrollAreaWidgetContents);
        MyImage->setObjectName(QStringLiteral("MyImage"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MyImage->sizePolicy().hasHeightForWidth());
        MyImage->setSizePolicy(sizePolicy);
        MyImage->setAlignment(Qt::AlignCenter);

        verticalLayout_4->addWidget(MyImage);

        ImageScrollArea->setWidget(ImageScrollAreaWidgetContents);

        horizontalLayout->addWidget(ImageScrollArea);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);

        PushButton = new QPushButton(centralWidget);
        PushButton->setObjectName(QStringLiteral("PushButton"));

        verticalLayout_5->addWidget(PushButton);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout_5);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setFrameShape(QFrame::NoFrame);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        InputName = new QLineEdit(centralWidget);
        InputName->setObjectName(QStringLiteral("InputName"));
        InputName->setEnabled(false);

        formLayout->setWidget(0, QFormLayout::FieldRole, InputName);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFrameShape(QFrame::NoFrame);

        formLayout->setWidget(1, QFormLayout::LabelRole, label_3);

        OutputName = new QLineEdit(centralWidget);
        OutputName->setObjectName(QStringLiteral("OutputName"));

        formLayout->setWidget(1, QFormLayout::FieldRole, OutputName);


        verticalLayout_3->addLayout(formLayout);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        ContentsScrollArea = new QScrollArea(groupBox);
        ContentsScrollArea->setObjectName(QStringLiteral("ContentsScrollArea"));
        ContentsScrollArea->setWidgetResizable(true);
        ContentsScrollAreaWidgetContents = new QWidget();
        ContentsScrollAreaWidgetContents->setObjectName(QStringLiteral("ContentsScrollAreaWidgetContents"));
        ContentsScrollAreaWidgetContents->setGeometry(QRect(0, 0, 434, 366));
        verticalLayout = new QVBoxLayout(ContentsScrollAreaWidgetContents);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        Contents = new QPlainTextEdit(ContentsScrollAreaWidgetContents);
        Contents->setObjectName(QStringLiteral("Contents"));

        verticalLayout->addWidget(Contents);

        ContentsScrollArea->setWidget(ContentsScrollAreaWidgetContents);

        verticalLayout_2->addWidget(ContentsScrollArea);


        verticalLayout_3->addWidget(groupBox);


        horizontalLayout->addLayout(verticalLayout_3);

        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_3);

        ResetButton = new QPushButton(centralWidget);
        ResetButton->setObjectName(QStringLiteral("ResetButton"));

        verticalLayout_6->addWidget(ResetButton);

        SaveButton = new QPushButton(centralWidget);
        SaveButton->setObjectName(QStringLiteral("SaveButton"));

        verticalLayout_6->addWidget(SaveButton);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_4);


        horizontalLayout->addLayout(verticalLayout_6);

        MainWindowClass->setCentralWidget(centralWidget);

        retranslateUi(MainWindowClass);
        QObject::connect(ResetButton, SIGNAL(clicked()), Contents, SLOT(clear()));

        QMetaObject::connectSlotsByName(MainWindowClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindowClass)
    {
        MainWindowClass->setWindowTitle(QApplication::translate("MainWindowClass", "MainWindow", Q_NULLPTR));
        MyImage->setText(QApplication::translate("MainWindowClass", "MyImage", Q_NULLPTR));
        PushButton->setText(QApplication::translate("MainWindowClass", "Push", Q_NULLPTR));
        label_2->setText(QApplication::translate("MainWindowClass", "Input", Q_NULLPTR));
        label_3->setText(QApplication::translate("MainWindowClass", "Ouput (.txt)", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("MainWindowClass", "Contents", Q_NULLPTR));
        ResetButton->setText(QApplication::translate("MainWindowClass", "Reset", Q_NULLPTR));
        SaveButton->setText(QApplication::translate("MainWindowClass", "Save", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindowClass: public Ui_MainWindowClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
