﻿#pragma once
#include <QLabel>

class MyImageLabel : public QLabel {
	Q_OBJECT

public:
	MyImageLabel(QWidget * parent = Q_NULLPTR);
	~MyImageLabel();
	void setImage(const QImage& image);

protected:
	virtual void resizeEvent(QResizeEvent * event) override;
	virtual void mousePressEvent(QMouseEvent * event) override;
	virtual void mouseMoveEvent(QMouseEvent * event) override;

private:
	void reset();

private:
	QImage m_image;
	QPoint m_pointA;
	QPoint m_pointB;
	
signals: void sendState(const QString& state);
	
public slots:
	void recvPushButton();
};
