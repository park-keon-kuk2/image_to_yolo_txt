﻿#include "MyImageLabel.h"
#include <QPixmap>
#include <QPainter>
#include <QMouseEvent>
#include <QtCore/qmath.h>

MyImageLabel::MyImageLabel(QWidget * parent) : QLabel(parent) {
	
}

MyImageLabel::~MyImageLabel() {
	
}

void MyImageLabel::setImage(const QImage & image)
{
	m_image = image;
	m_pointA = QPoint(0, 0);
	m_pointB = QPoint(width(), height());
	
	reset();
}

void MyImageLabel::resizeEvent(QResizeEvent * event)
{
	m_pointA = QPoint(0, 0);
	m_pointB = QPoint(width(), height());

	reset();
}

void MyImageLabel::mousePressEvent(QMouseEvent * event)
{
	if(event->button() == Qt::LeftButton)
		m_pointA = event->pos();
	if (event->button() == Qt::RightButton)
		m_pointB = event->pos();

	reset();
}

void MyImageLabel::mouseMoveEvent(QMouseEvent * event)
{
	if (event->buttons() & Qt::LeftButton)
		m_pointA = event->pos();
	if (event->buttons() & Qt::RightButton)
		m_pointB = event->pos();

	reset();
}

void MyImageLabel::reset()
{
	int w = this->width();
	int h = this->height();

	QPixmap pix = QPixmap::fromImage(m_image).scaled(w, h,
		Qt::AspectRatioMode::IgnoreAspectRatio,
		Qt::TransformationMode::SmoothTransformation);

	QPainter painter(&pix);

	painter.setCompositionMode(QPainter::RasterOp_SourceXorDestination);
	painter.setPen(QColor(0xff, 0xff, 0xff));

	painter.drawLine(QPoint(0, m_pointA.y()), QPoint(w, m_pointA.y()));
	painter.drawLine(QPoint(m_pointA.x(), 0), QPoint(m_pointA.x(), h));

	painter.drawLine(QPoint(0, m_pointB.y()), QPoint(w, m_pointB.y()));
	painter.drawLine(QPoint(m_pointB.x(), 0), QPoint(m_pointB.x(), h));

	this->setPixmap(pix);
}

void MyImageLabel::recvPushButton()
{
	float w = (float)this->width();
	float h = (float)this->height();

	float width = qAbs(m_pointA.x() - m_pointB.x());
	float height = qAbs(m_pointA.y() - m_pointB.y());
	float x = (float)(m_pointA.x() + m_pointB.x()) / 2.f;
	float y = (float)(m_pointA.y() + m_pointB.y()) / 2.f;

	QString state;
	state.sprintf("0 %f %f %f %f", x / w, y / h, width / w, height / h);
	
	sendState(state);
}
