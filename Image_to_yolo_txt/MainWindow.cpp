#include "MainWindow.h"
#include <QEvent>
#include <QMetaEnum>
#include <QDragMoveEvent>
#include <QMimeData>
#include <QMessageBox>
#include <QTextStream>
#include <QDir>

MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	// drop 할 수 있게
	setAcceptDrops(true);
	ui.centralWidget->setAcceptDrops(true);

	// 콘텐츠 사이즈 가능
	ui.MyImage->setScaledContents(true);
	ui.MyImage->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

	connect(ui.PushButton, &QPushButton::pressed, ui.MyImage, &MyImageLabel::recvPushButton);
	connect(ui.MyImage, &MyImageLabel::sendState, this, [this](const QString& state) {
		ui.Contents->appendPlainText(state);
	});

	connect(ui.SaveButton, &QPushButton::pressed, this, [this]() {
		QString filePath = m_fileInfo.absoluteDir().absolutePath() + "/" + ui.OutputName->text();
		
		QFile file(filePath);
		file.open(QIODevice::WriteOnly);

		QTextStream out(&file);
		out << ui.Contents->toPlainText();
	});
}

MainWindow::~MainWindow()
{
}

void MainWindow::dragEnterEvent(QDragEnterEvent * event)
{
	event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent * event)
{
	/* 여러가지 이미지 포맷을 사용하려면 pulgin에서 imageformat 폴더를 옮기면 됩니다. */
	/* https://stackoverflow.com/questions/14878670/qt-does-not-load-jpg-just-png */

	/* 지원하는 이미지 포맷 보는 방법 */
	/* #include <QImageReader> */
	/* auto formats = QImageReader::supportedImageFormats() */

	/* 파일 정보에서 파일 이름(+확장자) 얻기 */
	/* https://stackoverflow.com/questions/15244911/removing-extension-of-a-file-name-in-qt */

	QList<QUrl> urls = event->mimeData()->urls();

	if (urls.count() != 1) {
		QMessageBox mb;
		mb.setText("please drag a single file.");
		mb.exec();
		return;
	}

	QString absPath = urls[0].toLocalFile();
	m_fileInfo.setFile(absPath);
	QImage image(absPath);

	if (image.isNull())
	{
		QMessageBox mb;
		mb.setText("please drag a image file.");
		mb.exec();
		return;
	}

	ui.InputName->setText(m_fileInfo.fileName());
	ui.OutputName->setText(m_fileInfo.completeBaseName() + ".txt");
	ui.MyImage->setImage(image);
	ui.Contents->clear();
}